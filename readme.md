# Universal Function Tester
## Introducción
Universal Function Tester es una interface de tester estandarizada que busca facilitar el testeo de cualquier función creada en C, esta interface no incluye ningún tester en su repositorio base, es el usuario quien debe testear su software pero la interface debería facilitarlo al no obligar a diseñar un frontend.
### Como funciona
